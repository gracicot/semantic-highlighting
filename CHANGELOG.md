# Change Log

## Version 0.7.0

* Can toggle extension on/off with the command "Toggle semantic highlighting".

## Version 0.6.0

* Add support for all languages.

## Version 0.5.0

* Add a change log.
* Add setting to choose to highlight colors.

## Version 0.4.0

* Initial release.

## [Unreleased]

- Open to suggestions.